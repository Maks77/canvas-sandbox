import { RenderStyle, RenderStyleWithRadius } from "./render-style"

export class HelperRenderOptions {
    public showPivot = false
    public showBoundary = false

    private _pivotStyle: RenderStyleWithRadius
    private _boundaryStyle: RenderStyle

    constructor() {
        const pivotStyle = new RenderStyleWithRadius()
        pivotStyle.strokeColor = 'transparent'
        pivotStyle.fillColor = 'red'
        pivotStyle.radius = 2
        this._pivotStyle = pivotStyle

        const boundaryStyle = new RenderStyle()
        this._boundaryStyle = boundaryStyle
    }

    get pivotStyle(): RenderStyleWithRadius {
        return this._pivotStyle
    }

    get boundaryStyle(): RenderStyle {
        return this._boundaryStyle
    }
}
