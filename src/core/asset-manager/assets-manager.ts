export interface ImageAsset {
  name: string
  image: HTMLImageElement
}



export class AssetsManager {
  private imageAssets: ImageAsset[] = []

  constructor() {}

  public addImageAsset(name: string, fileName: string) {
    const image = new Image()
    image.src = AssetsManager.assetURL(fileName)
    this.imageAssets.push({
      name,
      image
    })
  }

  public getImageAssetByName(name: string): ImageAsset | undefined {
    const existedAsset = this.imageAssets.find(asset => asset.name === name)
    if (!existedAsset) {
      throw new Error(`Asset with name "${name}" not found`)
    }
    return existedAsset
  }

  static assetURL(filename: string, ext = 'png'): string {
    return `./assets/${filename}.${ext}`
  }

}
