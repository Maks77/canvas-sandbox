export interface ISpriteOptions {
  imageName: string
  sx: number
  sy: number
  width: number
  height: number
}

export class Sprite {
  private imageName: string
  private sx: number
  private sy: number
  public width: number
  public height: number

  constructor(options: ISpriteOptions) {
    const {imageName, sx, sy, width, height} = options
    this.imageName = imageName
    this.sx = sx
    this.sy = sy
    this.width = width
    this.height = height
  }

  get sourceX(): number {
    return this.sx
  }

  get sourceY(): number {
    return this.sy
  }

  setSourceXY(sx: number, sy: number) {
    this.sx = sx
    this.sy = sy
  }
}


