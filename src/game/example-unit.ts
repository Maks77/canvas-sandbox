import {GameObject} from "../core/common/game-object/game-object";
import {IGameObjectProps} from "../core/common/game-object/IGameObjectProps";
import {ISpriteFrame} from "../core/animations/ISpriteFrame";
import {Animation} from "../core/animations/animation";
import {AnimationManager} from "../core/animations/animation-manager";


const playerRunFrames: ISpriteFrame[] = []
for (let i = 0; i < 6; i++) {
  playerRunFrames.push({
    sx: i * 83,
    sy: 0,
    width: 75,
    height: 100
  })
}

export class ExampleUnit extends GameObject {
  activeAnimation: Animation
  animationManager: AnimationManager

  constructor(props: IGameObjectProps) {
    super(props);

    this.animationManager = new AnimationManager()
    this.animationManager.addAnimation('run', {imageName: 'player', frames: playerRunFrames, speed: 100})

    this.activeAnimation = this.animationManager.getAnimation('run')
  }

  public update(dt: number) {
    this.activeAnimation.update(dt)
  }
  public render(dt: number) {
    this.activeAnimation.render(
      this.ctx,
      this.gameContext.assetsManager.getImageAssetByName('player').image,
      this.position,
      this.size,
      dt
    )
  }
}
