import { Point } from "../../vector/types";
import { RenderableObject } from "./renderable-object";

export class CircleObject extends RenderableObject {
     private readonly _radius: number

    constructor(x: number, y: number, radius: number) {
        super(x, y)
        this._radius = radius
    }

    render(ctx: CanvasRenderingContext2D): void {
        ctx.arc(this.x, this.y, this._radius, 0, Math.PI * 2)
        ctx.fill()
        ctx.stroke()
    }

    getPivot(): Point {
        return {x: this.x, y: this.y}
    }
}
