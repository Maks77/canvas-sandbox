export class Size {
    private _width: number
    private _height: number

    constructor(width: number, height: number) {
        this._width = width
        this._height = height
    }

    public get width(): number {
        return this._width
    }
    public set width(value: number) {
        this._width = value
    }

    public get height(): number {
        return this._height
    }

    public set height(value: number) {
        this._height = value
    }

    public get halfWidth(): number {
        return this._width / 2
    }

    public get halfHeight(): number {
        return this._height / 2
    }
}