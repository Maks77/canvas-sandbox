import { Size } from "../size";
import { Point } from "../../vector/types";
import { RenderableObject } from "./renderable-object";

export class RectangleObject extends RenderableObject {
    private readonly _size: Size
    
    constructor(x: number, y: number, size: Size) {
        super(x, y)
        this._size = size
    }

    render(ctx: CanvasRenderingContext2D): void {
        ctx.fillRect(
            this.x,
            this.y,
            this.size.width,
            this.size.height
        )
        ctx.strokeRect(
            this.x,
            this.y,
            this.size.width,
            this.size.height
        )
    }

    get size(): Size {
        return this._size
    }

    getPivot(): Point {
        const centerX = this.x + (this._size.width / 2)
        const centerY = this.y + (this._size.height / 2)
        return {x: centerX, y: centerY}
    }
}
