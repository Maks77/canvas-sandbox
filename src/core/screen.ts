export class Screen {
    private canvas: HTMLCanvasElement
    private document: Document

    private _width = 500
    private _height = 500

    constructor(document: Document) {
        this.document = document
        this.init()
    }

    private init(): void {
        let canvasEl = this.getCanvasElement()
        canvasEl.width = this._width
        canvasEl.height = this._height
        canvasEl.style.border = '1px solid black'
        
        this.canvas = canvasEl
    }

    private getCanvasElement(): HTMLCanvasElement {
        const existedEl = this.document.getElementsByTagName('canvas')[0]
        if (existedEl) {
            return existedEl
        }

        const canvasEl = document.createElement('canvas')
        this.document.body.appendChild(canvasEl)
        return canvasEl
    }

    get width(): number {
        return this.canvas.width
    }

    get height(): number {
        return this.canvas.height
    }

    get ctx(): CanvasRenderingContext2D {
        return this.canvas.getContext('2d')
    }
}
