import {IGameContext, IRenderable} from "../common/interfaces";

export interface IRendererProps {
  gameContext: IGameContext
}

export class Renderer implements IRenderable {
  private gameContext: IGameContext
  private renderableObjects: IRenderable[] = []

  constructor(props: IRendererProps) {
    this.gameContext = props.gameContext
  }

  public addObject(obj: IRenderable) {
    this.renderableObjects.push(obj)
  }

  public addObjectList(objectList: IRenderable[]) {
    this.renderableObjects.push(...objectList)
  }

  public render(dt: number): void {
    for (let i = 0; i < this.renderableObjects.length; i++) {
      this.renderableObjects[i].render(dt)
    }
  }


}
