import {Sprite} from "./sprite";
import {ISpriteFrame} from "./ISpriteFrame";
import {Vector} from "../vector/vector";
import {Size} from "../common/size";

export interface IAnimationOptions {
  imageName: string,
  frames: ISpriteFrame[],
  speed: number,
  repeat?: boolean,
  autorun?: boolean
}

export class Animation extends Sprite {
  frames: ISpriteFrame[]
  currentFrameIndex = 0
  lastTime = 0
  speed: number
  running: boolean
  repeat = true
  onEnd: () => void

  // @ts-ignore
  constructor(options: IAnimationOptions) {
    const {imageName, frames, speed, repeat = true, autorun = true} = options
    super({
      imageName,
      sx: frames[0].sx,
      sy: frames[0].sy,
      width: frames[0].width,
      height: frames[0].height
    });
    this.frames = frames
    this.onEnd = () => {}
    this.repeat = repeat
    this.running = autorun
    this.speed = speed
  }

  get totalFrames(): number {
    return this.frames.length
  }

  public setFrame(index: number) {
    this.currentFrameIndex = index
    this.setSourceXY(this.frames[index].sx, this.frames[index].sy)
  }

  public run() {
    if (!this.running) {
      this.setFrame(0)
      this.running = true
    }
  }

  public stop() {
    this.running = false
  }

  private nextFrame() {
    if ((this.currentFrameIndex + 1) == this.totalFrames) {
      if (this.onEnd) {
        this.onEnd();
      }
      if (this.repeat) {
        this.setFrame(0);
        return;
      }
      this.stop();
      return;
    }
    this.setFrame(this.currentFrameIndex + 1);
  }

  public update(time: number) {
    if (!this.running) {
      return;
    }
    if (this.lastTime == 0) {
      this.lastTime = time;
      return;
    }
    if ((time - this.lastTime) > this.speed) {
      this.nextFrame();
      this.lastTime = time;
    }
  }

  public render(ctx: CanvasRenderingContext2D, image: HTMLImageElement, position: Vector, size: Size, dt: number) {
    ctx.drawImage(
      image,
      this.sourceX,
      this.sourceY,
      this.width,
      this.height,
      position.x,
      position.y,
      size.width,
      size.height
    )
  }
}
