import {Screen} from '../core/screen'
import {AssetsManager} from "../core/asset-manager/assets-manager";
import {ExampleUnit} from "./example-unit";
import {Vector} from "../core/vector/vector";
import {IGameContext} from "../core/common/interfaces";
import {Renderer} from "../core/renderer/renderer";

export class Game implements IGameContext {
  screen: Screen
  assetsManager: AssetsManager
  renderer: Renderer

  // objects
  unit: ExampleUnit

  constructor() {
    this.screen = new Screen(document)
    this.assetsManager = new AssetsManager()
    this.renderer = new Renderer({gameContext: this})

    this.initObjects()
  }

  private initObjects(): void {
    this.assetsManager.addImageAsset('player', 'player')
    this.unit = new ExampleUnit({
      gameContext: this,
      position: new Vector(250, 250),
      width: 50,
      height: 80
    })


    this.renderer.addObject(this.unit)
  }

  public start() {
    this.loop(0)
  }

  private loop(dt: number): void {
    this.screen.ctx.clearRect(0, 0, this.screen.width, this.screen.height)
    this.update(dt)
    this.render(dt)
    window.requestAnimationFrame(this.loop.bind(this))
  }

  private update(dt: number): void {
    this.unit.update(dt)
  }

  private render(dt: number): void {
    this.renderer.render(dt)
  }


}
