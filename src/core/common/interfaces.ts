import {AssetsManager} from "../asset-manager/assets-manager";
import {Screen} from '../screen'

export interface IGameContext {
  screen: Screen,
  assetsManager: AssetsManager
}

export interface IUpdateble {
  update(dt: number): void
}

export interface IRenderable {
  render(dt: number): void
}
