import {Animation, IAnimationOptions} from "./animation";

export class AnimationManager {
  private animationMap = new Map<string, Animation>()

  public addAnimation(name: string, options: IAnimationOptions) {
    const animation = new Animation(options)
    this.animationMap.set(name, animation)
  }

  public getAnimation(name: string): Animation {
    const existedAnimation = this.animationMap.get(name)
    if (!existedAnimation) {
      throw new Error(`Animation "${name}" not found`)
    }
    return existedAnimation
  }

}
