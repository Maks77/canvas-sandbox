import {Vector} from "../../vector/vector";
import {IGameContext} from "../interfaces";

export interface IGameObjectProps {
  gameContext: IGameContext
  position: Vector
  width: number
  height: number
}
