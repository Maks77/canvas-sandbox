export interface ISpriteFrame {
  sx: number,
  sy: number,
  width: number,
  height: number
}
