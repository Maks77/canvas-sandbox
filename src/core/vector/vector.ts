import { Point } from "./types";

export class Vector {
    private _x: number;
    private _y: number;
  
    constructor(x: number, y: number) {
      this._x = x;
      this._y = y;
    }
  
    set x(value: number) {
        this._x = value
    }

    set y(value: number) {
        this._y = value
    }

    get x(): number {
      return this._x
    }

    get y(): number {
      return this._y
    }

    get asPoint(): Point {
        return {x: this._x, y: this._y}
    }
  
    add(vector: Vector): Vector {
      return new Vector(this._x + vector._x, this._y + vector._y)
    }
  
    subtract(vector: Vector): Vector {
      return new Vector(this._x -= vector._x, this._y -= vector._y)
    }
  
    multiply(n: number): Vector {
      return new Vector(this._x *= n, this._y *= n);
    }
  
    length(): number {
      return Math.sqrt(this._x * this._x + this._y * this._y);
    }
  
    cross(vector: Vector): number {
      return this._x * vector._y - this._y * vector._x;
    }
  
    dot(vector: Vector): number {
      return this._x * vector._x + this._y * vector._y
    }
  
    normalize(): Vector {
      return new Vector(this._x / this.length(), this._y / this.length())
    }
  
  }
