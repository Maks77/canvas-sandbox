import { Point } from "../../vector/types"
import { HelperRenderOptions } from "./helpers/helper-render-options"
import { RenderStyle, RenderStyleWithRadius } from "./helpers/render-style"

export abstract class RenderableObject {
    private _x: number
    private _y: number
    private readonly _style: RenderStyle
    private readonly _helperRenderOptions: HelperRenderOptions

    protected constructor(x: number, y: number, style = new RenderStyle()) {
        this._x = x
        this._y = y
        this._style = style
        this._helperRenderOptions = new HelperRenderOptions()
    }

    set x(value: number) {
        this._x = value
    }

    set y(value: number) {
        this._y = value
    }

    get x(): number {
        return this._x
    }

    get y(): number {
        return this._y
    }

    get style(): RenderStyle {
        return this._style
    }

    get helperRenderOptions(): HelperRenderOptions {
        return this._helperRenderOptions
    }

    protected useRenderStyle(ctx: CanvasRenderingContext2D): void {
        ctx.strokeStyle = this.style.strokeColor
        ctx.fillStyle = this.style.fillColor
        ctx.lineWidth = this.style.lineWidth
    }

    protected showPivot(ctx: CanvasRenderingContext2D, pivotStyle: RenderStyleWithRadius): void {
        const {x, y} = this.getPivot()
        ctx.save()
        ctx.beginPath()
        ctx.fillStyle = pivotStyle.fillColor
        ctx.strokeStyle = pivotStyle.strokeColor
        ctx.lineWidth = pivotStyle.lineWidth
        ctx.arc(x, y, pivotStyle.radius, 0, Math.PI * 2)
        ctx.fill()
        ctx.stroke()
        ctx.closePath()
        ctx.restore()
    }

    public systemRender(ctx: CanvasRenderingContext2D): void {
        ctx.save()
        ctx.beginPath()
        this.useRenderStyle(ctx)
        ctx.translate(this.x, this.y)
        this.render(ctx)

        if (this._helperRenderOptions.showPivot) {
            this.showPivot(ctx, this.helperRenderOptions.pivotStyle)
        }
        ctx.closePath()
        ctx.restore()
    }

    abstract render(ctx: CanvasRenderingContext2D): void
    abstract getPivot(): Point
}
