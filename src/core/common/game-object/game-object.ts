import {Vector} from "../../vector/vector";
import {IGameObjectProps} from "./IGameObjectProps";
import {IGameContext, IRenderable, IUpdateble} from "../interfaces";
import {Size} from "../size";

export abstract class GameObject implements IUpdateble, IRenderable {
  gameContext: IGameContext
  position: Vector
  size: Size

  protected constructor(props: IGameObjectProps) {
    this.gameContext = props.gameContext
    this.position = props.position
    this.size = new Size(props.width, props.height)
  }

  get ctx(): CanvasRenderingContext2D {
    return this.gameContext.screen.ctx
  }

  abstract update(dt: number): void

  abstract render(dt: number): void
}
