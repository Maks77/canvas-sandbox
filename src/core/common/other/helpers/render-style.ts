export class RenderStyle {
    public fillColor = 'black'
    public strokeColor = 'black'
    public lineWidth = 1
}

export class RenderStyleWithRadius extends RenderStyle {
    public radius: number
}